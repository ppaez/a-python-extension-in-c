#define PY_SSIZE_T_CLEAN
#include <Python.h>

static char module_doc[] = "This is a C extension\n\n"
  "A very simple extension.";

static PyObject *
system_wrap(PyObject *self, PyObject *args)
{
    const char *command;
    int status;

    if (!PyArg_ParseTuple(args, "s", &command))
        return NULL;
    status = system(command);
    if (status < 0) {
        return NULL;
    }
    return PyLong_FromLong(status);
}

static PyMethodDef methods[] = {
    {"system",  system_wrap, METH_VARARGS,
     "Execute a shell command.\n\nReturns the exit code."},
    {NULL, NULL, 0, NULL}
};


static struct PyModuleDef moduledef = {
    PyModuleDef_HEAD_INIT,
    "module",
    module_doc,
    -1,
    methods
};

PyMODINIT_FUNC
PyInit_module(void)
{
    return PyModule_Create(&moduledef);
}
