from setuptools import setup, Extension

module = Extension('module',
                    sources = ['module.c'])

setup (ext_modules = [module])
